import 'package:flutter/material.dart';
import 'package:url/style.dart';
import 'package:url_launcher/url_launcher.dart';
class UrlLauncher extends StatefulWidget {
  const UrlLauncher({Key? key}) : super(key: key);

  @override
  State<UrlLauncher> createState() => _UrlLauncherState();
}

class _UrlLauncherState extends State<UrlLauncher> {
  TextEditingController quantityController = TextEditingController();
  int quantity = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,

                children: [
                  MaterialButton(
                      minWidth: 150,
                      color: Colors.indigo,
                      onPressed: ( ){
                        final url=Uri.parse('https://www.facebook.com/naim.hohen');
                        _LaunchUrl(url);
                      }, child: Text("Url_Launcher",style:CustomStyle.textStyle,)),
                  SizedBox(height: 15,),
                  MaterialButton(
                      minWidth: 150,
                      color: Colors.indigo,
                      onPressed: ( ){
                        String Number="01755428752";
                        final url=Uri.parse('tel:$Number');
                        _LaunchUrl(url);
                      }, child: Text("Call Me",style: CustomStyle.textStyle,)),
                  SizedBox(height: 15,),
                  MaterialButton(
                      minWidth: 150,

                      color: Colors.indigo,
                      onPressed: ( ){
                        String Number="01755428752";
                        final url=Uri.parse('sms:$Number');
                        _LaunchUrl(url);
                      }, child: Text("SMS",style:CustomStyle.textStyle,)),

                ],
              ),
            )
        )
    );
  }
  Future<void> _LaunchUrl(Uri url)async{
    if(await launchUrl(url)){
      throw"$url";
    }
  }
}
